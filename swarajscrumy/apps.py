from django.apps import AppConfig


class SwarajscrumyConfig(AppConfig):
    name = 'swarajscrumy'
